# Selar Scpit

*如无特别标注，所有脚本都是针对CentOS 7*

## How To Use
    wget -c https://gitlab.com/liyuan.ly/selar-script/-/archive/master/selar-script-master.tar.gz
    tar -zxf selar-script-master.tar.gz
    rm -f selar-script-master.tar.gz
    nohup ./selar-script-master/use/Build_Server_Web.sh & # what you want use
    rm -rf selar-script-master

## 脚本之间的依赖关系

### SelarServerInstall.sh
- GitInstall.sh
- GoInstall.sh
### SelarWebInstall.sh
- GitInstall.sh
- NodeJSInstall.sh
- NginxInstall.sh

## PS
- ### use 文件中的脚本是通过调用其他脚本实现的
- ### 所有脚本均未考虑错误运行的情况

## Todo:

### Selar-Server
- mysql的安装、导入、导出
- 配置文件修改

### Selar-Web
- 配置文件修改