#!/bin/bash
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Start[$(basename $0 .sh)]" >> ~/selar_script_log
source /etc/profile
mkdir -p ${GOPATH}/src/gitlab.com/liyuan.ly
cd ${GOPATH}/src/gitlab.com/liyuan.ly
git clone git@gitlab.com:liyuan.ly/selar-server
cd selar-server
chmod +x ./script/build.sh
./script/build.sh
nohup ./output/server/exe -config-file ./output/server/conf.yml 2>&1 > ./output/server/log &
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Done [$(basename $0 .sh)]" >> ~/selar_script_log