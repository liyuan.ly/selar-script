#!/bin/bash
# this script would cost more about 55 mins
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Start[$(basename $0 .sh)]" >> ~/selar_script_log
version="10.14.2"
#----------------------------------------------------------------------------
yum install -y gcc gcc-c++
wget -c https://nodejs.org/dist/v${version}/node-v${version}.tar.gz
tar -zxf node-v${version}.tar.gz
rm -f node-v${version}.tar.gz
cd node-v${version}
./configure
make
make install
cd ..
rm -rf node-v${version}
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Done [$(basename $0 .sh)]" >> ~/selar_script_log