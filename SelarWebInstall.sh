#!/bin/bash
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Start[$(basename $0 .sh)]" >> ~/selar_script_log
source /etc/profile
cd ~
git clone git@gitlab.com:liyuan.ly/selar-web
cd selar-web
npm install
npm run build
cp index.html ${NGINX_HOME}/html
cp -R dist ${NGINX_HOME}/html
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Done [$(basename $0 .sh)]" >> ~/selar_script_log