#!/bin/bash
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Start[$(basename $0 .sh)]" >> ~/selar_script_log
version="1.14.2"
#----------------------------------------------------------------------------
yum install -y gcc gcc-c++ 
yum install -y pcre pcre-devel
yum install -y zlib zlib-devel
yum install -y openssl openssl-devel
wget -c https://nginx.org/download/nginx-${version}.tar.gz
tar -zxvf nginx-${version}.tar.gz
rm -f nginx-${version}.tar.gz
cd nginx-${version}
./configure
make
make install
cd ..
rm -rf nginx-${version}
echo 'export NGINX_HOME=/usr/local/nginx' >> /etc/profile
echo 'export PATH=$PATH:$NGINX_HOME/sbin' >> /etc/profile
source /etc/profile
echo -e 'worker_processes  1;\nevents {\n    worker_connections  1024;\n}\nhttp {\n    include       mime.types;\n    default_type  application/octet-stream;\n    sendfile        on;\n    keepalive_timeout  65;\n    gzip  on;\n    server {\n        listen       80;\n        server_name  localhost;\n        location / {\n            root   html;\n            index  index.html;\n            try_files $uri $uri/ @router;\n        }\n        location @router {\n            rewrite ^.*$ /index.html break;\n        }\n    }\n}' > $NGINX_HOME/conf/nginx.conf
nginx
echo "$NGINX_HOME/sbin/nginx" >> /etc/rc.local
rm -rf "$NGINX_HOME/html/*"
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Done [$(basename $0 .sh)]" >> ~/selar_script_log