# Nginx 静态资源服务器 安装、配置

## 安装 Nginx

[参考博客](https://www.cnblogs.com/kaid/p/7640723.html)

### 1. 安装 gcc

    yum install -y gcc-c++ 

### 2. 安装 PCRE pcre-devel

    yum install -y pcre pcre-devel

### 3. 安装 zlib

    yum install -y zlib zlib-devel

### 4. 安装 OpenSSL

    yum install -y openssl openssl-devel

### 5. 下载 nginx

    wget -c https://nginx.org/download/nginx-1.14.2.tar.gz

### 6. 解压 nginx

    tar -zxvf nginx-1.14.2

### 7. 使用默认配置

    cd nginx-1.14.2
    ./configure

### 8. 编译安装 nginx

    make
    make install

### 9. 添加环境变量
安装路径可以通过`whereis nginx`查看，不做特殊设置应该是`/usr/local/nginx`

    echo 'export NGINX_HOME=/usr/local/nginx' >> /etc/profile
    echo 'export PATH=$PATH:$NGINX_HOME/sbin' >> /etc/profile
    source /etc/profile

### 9. 启动、停止nginx
    nginx 
    nginx -s quit # 此方式停止步骤是待nginx进程处理任务完毕进行停止。
    nginx -s stop # 此方式相当于先查出nginx进程id再使用kill命令强制杀掉进程
    nginx -s reload
重启需要先quit再启动，修改nginx.conf可以通过reload在不退出nginx的情况下生效。

### 10. 开机自启
    
    echo "/usr/local/nginx/sbin/nginx" >> /etc/rc.local

## 配置

### SPA配置

    worker_processes  1;
    events {
        worker_connections  1024;
    }
    http {
        include       mime.types;
        default_type  application/octet-stream;
        sendfile        on;
        keepalive_timeout  65;
        gzip  on;
        server {
            listen       80;
            server_name  localhost;
            location / {
                root   html;
                index  index.html;
                try_files $uri $uri/ @router;
            }
            location @router {
                rewrite ^.*$ /index.html break;
            }
        }
    }