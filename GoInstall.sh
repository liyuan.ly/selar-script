#!/bin/bash
# 安装golang
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Start[$(basename $0 .sh)]" >> ~/selar_script_log
_goroot="/usr/local/go"
_gopath="/root/gopath"
_goversion="1.11.4"
#----------------------------------------------------------------------------
wget -c https://dl.google.com/go/go${_goversion}.linux-amd64.tar.gz
tar -zxf go${_goversion}.linux-amd64.tar.gz  -C $(dirname $_goroot)
rm -f go${_goversion}.linux-amd64.tar.gz
mkdir -p ${_gopath}/bin
mkdir -p ${_gopath}/pkg
mkdir -p ${_gopath}/src
echo "export GOROOT=$_goroot" >> /etc/profile
echo "export GOPATH=$_gopath" >> /etc/profile
echo 'export PATH=$PATH:$GOROOT/bin:$GOPATH/bin' >> /etc/profile
echo "$(date +'%Y-%m-%d %H:%M:%S') Script Done [$(basename $0 .sh)]" >> ~/selar_script_log