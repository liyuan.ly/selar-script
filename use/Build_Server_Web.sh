#!/bin/bash
ScriptDir=$(dirname $(cd $(dirname $0); pwd))
cd ~
{
    $ScriptDir/GitInstall.sh
    $ScriptDir/GoInstall.sh
    $ScriptDir/SelarServerInstall.sh
} &

$ScriptDir/NginxInstall.sh
$ScriptDir/NodeJSInstall.sh
$ScriptDir/SelarWebInstall.sh