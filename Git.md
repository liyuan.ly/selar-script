# GitLab Private Repository 需要权限
因为GitLab中selar-web和selar-server都是private仓库，需要权限才能clone，所以需要给定有权限的id_rsa，考虑id_ras和id_ras.pub是对应的，所以一并设置。

## id_rsa
    -----BEGIN RSA PRIVATE KEY-----
    MIIEowIBAAKCAQEAq36sstQdop0sCcurQ0Il+CX1zVkIeqvss4W+/2NFP7HohM3T
    nhus4GQymY7R2fIzbq0kY1wDiw0UODZToE8+xDhVFc+VeYO9V59HcOFWLaPWSl43
    27jtZxmEuw4jwkR+iWxD3Mpbz1EWkTMfAEqpQ9AqPGbAm/PyqnaELFEi6LEOFoKF
    Hp68KiKWGI7M/MhkU+sjtQIx5ZccZ/PYYqAoaV30pIs241o9wX1q/n9ZE6rqm8c6
    TCHkg7RSRVUk70HETYavPt4CV9r5p9ZgW68iz0W/Pqg/JsMw/Rr3JYpd4YEnaw68
    RaEAdD0ResE9B9mFb3igfwfc9FO88NKo5NGY0QIDAQABAoIBAF2sxfywrLdkjueS
    3qvQeBkB8zAF0AHh41t0VQ3/VZYjQdU48LzOdTHkyVbjknEwQTPVU6kKW80aic7i
    re9FGg3oj2FokEdd1JEGKenYzFAFzEm3CV/1tYe5gq0CXjoBW/UItFDoIBowOtl5
    SRfJsIegL1Hq9o65Q/Yk4rGqaB0dpEX/tgnisI/xOKDTFrM+ebls3b0nShA7a6gc
    p6EjzIIq7W/BKUI57vIFUTX42ML0YJGe/FGl0qEFgKVqu4UQNa9KaFxfBN/xJTVA
    v8PQwz1EjxmtBnQZhy5mqEduvt3iMzWpxvZ4xFL8apwE+PHSXnDtsJyISEv06Zjb
    A2ZqexkCgYEA3aieAxmoyHU/iag78tyNeEQzfofNwA3d+dUtFKbrWYnkwkDaBAGu
    GbJjaVz81Az+GH/e7RcWwp1GLS98WHTg9MfVpwDJSSN22SXBw6fc/a7rKwQWtRpf
    gffj6AfwXGlCvskvvdax1IaKOLfuBjl18/55KNlMTpPjAMhnR/2b+q8CgYEAxhB4
    DUy9WpgVUVVdkWjkg5F3Fv7bhHtygp4osgrjjRTwqxGezGxIkqy2hhsCSu5LpzOc
    HNw9CCKkiiAxJxmYYnvWIqOpXDP9keyFFFpXQChoVdqgabPUHt3BdZBJKn++tVoN
    IRJ4f46NonOQUIQxTq62UEG81mNSIYHDPJ8yhH8CgYEAs4KB7TrTkPw7Z61CV428
    WGDLXzZ2L4pLca/t4WLJCOt+wr6okUQXDiKFFehDrSuUgJzWGiZvvU6HbcKeLjkB
    kdtgsO4XFU9OSc+lG2IPu7b0JxMZnLNuE6CBBwGe7AgnEMF4E4MS423N9N8oOgxA
    9UfqMWXBwYVM+u5RjmDZ+GsCgYBdfnitDoRzt0aY1gsNEM6NrKTinftRVlR/g9kB
    8lHnzvKsTez/4j1/vzeJpkY2yLW9f4c5uFy28mNpMDkDk/lyw7IZN9hCReh0cYmA
    SgTVrD1UCZefTdvbliI0NJ/VB+gmAxQfgHCRBeGlxZdh6oS794zn5aAWM1wZpP9c
    JmnaUQKBgCyl944nne52P2PjsBCDG+Ax0uAssDkoDo13UW740iLuBySLFc5KyE9N
    fMf326yoF8Lv0nGhxpgBFRLDJALHgH5u9I5unGT/bvMKgpihFAAqjCHfXFBrs59B
    0K3SqaDglXHWPHTbRVkvmudzpZ4waBlDlriVq7ntdIt4WfDLnTGb
    -----END RSA PRIVATE KEY-----

### id_rsa文件最好权限为 0600
    
    chmod 0600 ~/.ssh/id_rsa

## id_ras.pub

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrfqyy1B2inSwJy6tDQiX4JfXNWQh6q+yzhb7/Y0U/seiEzdOeG6zgZDKZjtHZ8jNurSRjXAOLDRQ4NlOgTz7EOFUVz5V5g71Xn0dw4VYto9ZKXjfbuO1nGYS7DiPCRH6JbEPcylvPURaRMx8ASqlD0Co8ZsCb8/KqdoQsUSLosQ4WgoUenrwqIpYYjsz8yGRT6yO1AjHllxxn89hioChpXfSkizbjWj3BfWr+f1kTquqbxzpMIeSDtFJFVSTvQcRNhq8+3gJX2vmn1mBbryLPRb8+qD8mwzD9Gvclil3hgSdrDrxFoQB0PRF6wT0H2YVveKB/B9z0U7zw0qjk0ZjR root@VM_0_6_centos

## 初次从GitLab Clone需要添加known_hosts，为避免手动输入，使用如下命令

    ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

# Clone 命令

## Selar-Server

    git clone git@gitlab.com:liyuan.ly/selar-server

## Selar-Web

    git clone git@gitlab.com:liyuan.ly/selar-web